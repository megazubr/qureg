$.qureg = { 
    init: function ( $login, $password, $name ) {
        if (typeof($name) === 'undefined'){ $name = "";}
        if (typeof($login) === 'undefined'){ $login = "";}
        //if (typeof($phone) === 'undefined'){ $phone = "";}
        if (typeof($password) === 'undefined'){ console.log('Error in qureg plugin, no password'); return}
        var form = $('#checkout-contact-form .wa-form');
        form.find('.wa-field').each(function () {
			if ($(this).hasClass('wa-field-address')){
				return true;
			}
            $(this).removeClass('wa-required');
            $(this).hide();
        });
        $('#checkout-contact-form .wa-auth-adapters').remove();
        var login = form.find("input[name='login']");
        var pass = form.find("input[name='password']");
        if ( $('#create-user').is(':not(:checked)') ){
            $('#create-user').prop('checked','checked');
        }
        var create_form = $('#create-user-div');
        create_form.show();
        login.parents('.wa-field').show();
        pass.prop('disabled','').val($password);
        pass.attr('value',pass.val());
        login.prop('disabled','');
        login.val($login);
        pass.parent().parent().hide();

        var customer_name = $('input[name="customer\[name\]]');
        if( typeof(customer_name) !== "undefined" ){
            //create_form.prepend('<div class="wa-field"><div class="wa-name">Телефон</div><div class="wa-value"><input type="text" name="customer[phone]" placeholder="89123456789" value="' + $phone + '"/></div></div>');
            create_form.prepend('<div class="wa-field"><div class="wa-name">Имя</div><div class="wa-value"><input type="text" name="customer[name]" value="' + $name + '"/></div></div>');
        }
        console.log("pass: " + pass.val());
    }
};