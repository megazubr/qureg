<?php
/*
 * @author Nikolay Ivanov <megazubr@gmail.com>
 */
class shopQuregPlugin extends shopPlugin
{
    /**
     * Отключает "обязательные" поля в контактной форме чекаута и подключает управляющий js-код в шаблон чекаута.
     * @param array $param
     * @return html
     */
    public function frontendCheckout($param) {
        $step = ifset($param['step']);
        if($step && $step == 'contactinfo') {

            $app_settings_model = new waAppSettingsModel();

            $hash = md5_file(wa('shop')->getConfig()->getConfigPath('checkout.php', true, 'shop'));
            $old_hash = $app_settings_model->get(array('shop', 'qureg'),'checkout_file_hash');
            if ($hash != $old_hash)
            {
                $checkout_config = include(wa('shop')->getConfig()->getConfigPath('checkout.php', true, 'shop'));
                $save = false;
                if( is_array($checkout_config) && isset($checkout_config[$step]['fields']) ){
                    foreach ($checkout_config[$step]['fields'] as $key=>&$field){
                        if ( isset($field['fields']) && is_array($field['fields']) ) {
                            $this->checkRequire($field, $save);
                        } else if (isset($field['required']) && $field['required'] == "1"){
                            $field['required'] = "";
                            $save = true;
                        }
                    }
                    unset($field);
                }
                if ($save){
                    waUtils::varExportToFile($checkout_config, wa('shop')->getConfig()->getConfigPath('checkout.php', true, 'shop'));
                }
                $hash = md5_file(wa('shop')->getConfig()->getConfigPath('checkout.php', true, 'shop'));
                $app_settings_model->set(array('shop', 'qureg'),'checkout_file_hash', $hash);
            }

            $data = wa()->getStorage()->get('shop/checkout');


            $password = $data['contact_pass'];
            if(!$password){
                $password = $this->generatePassword();
                wa()->getStorage()->set(array('shop/checkout', 'contact_pass'), $password);
            }
            if (!empty($data['contact'])) {
                $login = $data['contact']->get('email', 'default');
                $name = $data['contact']->get('name');
                //$phone = $data['contact']->get('phone');
            } else {
                $login = $name = $phone = null;
            }
            $plugin_url = wa()->getPlugin('qureg')->getPluginStaticUrl();
            $root_url = wa()->getAppPath();

            $view = wa()->getView();
            $view->assign('plugin_url',$plugin_url);
            $view->assign('login',ifset($login));
            //$view->assign('phone',ifset($phone));
            $view->assign('name',ifset($name));
            $view->assign('password',$password);
            $tmpl = $view->fetch($root_url."/plugins/qureg/templates/actions/qureg.html", true);
            return $tmpl;
        }
    }
    /**
     * генерирует пароль
     * @param int $length
     * @param boolean $add_dashes
     * @return string
     */
    private function generatePassword($length = 9, $add_dashes = false){

	$sets = array();
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
		$sets[] = '23456789';
                //$sets[] = '!@#$%&*?';
	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}
	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];
	$password = str_shuffle($password);
	if(!$add_dashes)
		return $password;
	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
    }

    /**
     * Срабатывает при создании заказа(подтверждение заказа) - если требуется(отключено обязательное подтверждение акаунта), то высылает письмо с паролем.
     * @param array $param
     * @return true
     */
    public function createOrder($param) {
        $data = wa()->getStorage()->get('shop/checkout');
        $auth_config = wa('site')->getAuthConfig(siteHelper::getDomain());
        $confirm_email = false;
        if ( !empty($auth_config['params']) && !empty($auth_config['params']['confirm_email']) ){
            $confirm_email = $auth_config['params']['confirm_email'];
        }
        if (!$confirm_email) {
            $password = ifset($data['contact_pass']);
            $login = $data['contact']->get('email', 'default');
            $login = ifset($login);
            $name = $data['contact']->get('name');
            $name = ifset($name);
            if (!$password || !$login){
                new waException("Invalid parameters");
            }
            $subject = 'Ваш пароль в личный кабинет';
            $body = "Личный кабинет: http://dev.lcoal/my/</br >Логин: $login<br />Пароль: $password";

            $mail = new waMailMessage($subject, $body);
            $mail->addFrom(wa('shop')->getSetting('email'));
            $mail->addTo($login);
            $mail->send();
        }
        return;
    }

    private function checkRequire(&$checked_array, &$save) {
        foreach ($checked_array['fields'] as &$field){
            if ( isset($field['required']) && $field['required'] == "1")
            {
                $field['required'] = "";
            }
            $save = true;
        }
        unset($field);
    }
}

