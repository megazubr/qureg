<?php
/*
 * @author Nikolay Ivanov <megazubr@gmail.com>
 */
return array(
    'name' => 'Quick reg',
    'description' => 'Quick reg',
    //'img' => '',
    'vendor' => 835071,
    'version' => '0.1',
    'shop_settings' => true,
    'frontend' => false,
        'handlers' => array(
            'frontend_checkout' => 'frontendCheckout',
            'order_action.create' => 'createOrder'
        ),
    );